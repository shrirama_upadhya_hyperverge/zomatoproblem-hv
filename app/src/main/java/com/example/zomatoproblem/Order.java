package com.example.zomatoproblem;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

@Entity(tableName = "orders")
@TypeConverters(DateConverter.class)
public class Order {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "resaurant_name")
    private String restaurantName;

    @ColumnInfo(name = "items")
    private final String items;

    @ColumnInfo(name = "order_date")
    private final Date orderDate;

    @ColumnInfo(name = "amount")
    private final int amount;

    public Order(String restaurantName, String items, Date orderDate, int amount){
        this.restaurantName = restaurantName;
        this.items = items;
        this.orderDate = orderDate;
        this.amount = amount;
    }


    // Getters
    public int getId(){
        return id;
    }

    public String getRestaurantName(){
        return restaurantName;
    }

    public String getItems(){
        return items;
    }

    public Date getOrderDate(){
        return orderDate;
    }

    public int getAmount(){
        return amount;
    }


    // Setters
    public void setId(int id){
        this.id = id;
    }

    public void setRestaurantName(String restaurantName){
        this.restaurantName = restaurantName;
    }
}

