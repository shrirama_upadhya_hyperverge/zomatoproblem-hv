package com.example.zomatoproblem.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.zomatoproblem.HomeScreen;
import com.example.zomatoproblem.MapsFragment;
import com.example.zomatoproblem.R;
import com.example.zomatoproblem.RestaurantMenuActivity;
import com.example.zomatoproblem.TempRestaurant;
import com.example.zomatoproblem.apis.models.geoCodeResponse.Location;
import com.example.zomatoproblem.apis.models.geoCodeResponse.NearbyRestaurant;
import com.example.zomatoproblem.apis.models.geoCodeResponse.POJOGeoCodeResponseBody;
import com.example.zomatoproblem.apis.models.geoCodeResponse.Popularity;
import com.example.zomatoproblem.apis.models.locationDetailsResponse.BestRatedRestaurant;
import com.example.zomatoproblem.apis.models.locationDetailsResponse.POJOLocationDetailsResponseBody;
import com.example.zomatoproblem.apis.models.locationDetailsResponse.Restaurant;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private boolean sortValue = true; // Descending order
    private LinearLayout linearLayout;

    private ImageView locationIcon;
    ProgressBar progressBar;
    NestedScrollView nestedScrollView;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        locationIcon = root.findViewById(R.id.location);
        progressBar = root.findViewById(R.id.progressBarHomeFrag);
        nestedScrollView = root.findViewById(R.id.recycler_view_home_fragment);
        TextView localityText = root.findViewById(R.id.locality);
        localityText.setText(HomeScreen.locality);
        locationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMapFragment(getActivity().getApplicationContext());
            }
        });
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!HomeScreen.gotCurrentLoc)
                    handler.postDelayed(this, 1000);
                else {
                    // do actions
                    Log.e("onCreateVIew", "calling geocodeTask");

                    new GetGeoCodeTask().execute();
                }
            }
        }, 1000);

        return root;
    }


    private void openMapFragment(Context root) {
        getActivity().findViewById(R.id.location).setClickable(false);
        getActivity().findViewById(R.id.filterResultButton).setClickable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MapsFragment nextFrag = new MapsFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.map_layout, nextFrag, "mapFragment")
                        .addToBackStack(null)
                        .commit();
            }
        }, 500);

    }


    public void callLocationDetails(int entity_id, String entity_type) {
        Call<POJOLocationDetailsResponseBody> call = HomeScreen.zomatoAPI.getLocationDetails(entity_id, entity_type);
        call.enqueue(new Callback<POJOLocationDetailsResponseBody>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<POJOLocationDetailsResponseBody> call, Response<POJOLocationDetailsResponseBody> response) {
                //Main Response Object
                POJOLocationDetailsResponseBody pOJOLocationDetailsResponseBody = response.body();
                //1.Location
                com.example.zomatoproblem.apis.models.locationDetailsResponse.Location location = pOJOLocationDetailsResponseBody.getLocation();
                //2.Best rated Restaurants
                List<BestRatedRestaurant> bestRatedRestaurants = pOJOLocationDetailsResponseBody.getBest_rated_restaurant();
                ImageView filterImage = getActivity().findViewById(R.id.filterResultButton);
                callSort(sortValue, bestRatedRestaurants, getActivity());
                filterImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sortValue = !sortValue;
                        linearLayout.removeAllViews();
                        callSort(sortValue, bestRatedRestaurants, getActivity());
                    }
                });
            }

            @Override
            public void onFailure(Call<POJOLocationDetailsResponseBody> call, Throwable t) {
                Toast.makeText(getActivity(), "Cache has been cleared / max-stale has been reached! Please check your connection", Toast.LENGTH_LONG).show();
                Log.e("App", t.getMessage());
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void callSort(boolean sortValue, List<BestRatedRestaurant> bestRatedRestaurants, FragmentActivity root) {
        getActivity().findViewById(R.id.progressBarHomeFrag).setVisibility(View.GONE);
        getActivity().findViewById(R.id.linear_layout_for_restaurant_cards).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.recycler_view_home_fragment).setVisibility(View.VISIBLE);
        if (sortValue) {
            bestRatedRestaurants.sort((o1, o2) -> {
                if (Double.parseDouble(o1.getRestaurant().getUser_rating().getAggregate_rating()) > Double.parseDouble(o2.getRestaurant().getUser_rating().getAggregate_rating())) {
                    return -1;
                } else if (Double.parseDouble(o1.getRestaurant().getUser_rating().getAggregate_rating()) < Double.parseDouble(o2.getRestaurant().getUser_rating().getAggregate_rating())) {
                    return 1;
                } else {
                    return 0;
                }
            });
            Toast.makeText(getActivity().getApplicationContext(), "Descending order (feature : Rating)", Toast.LENGTH_SHORT).show();
        } else {
            bestRatedRestaurants.sort((o1, o2) -> {
                if (Double.parseDouble(o1.getRestaurant().getUser_rating().getAggregate_rating()) > Double.parseDouble(o2.getRestaurant().getUser_rating().getAggregate_rating())) {
                    return 1;
                } else if (Double.parseDouble(o1.getRestaurant().getUser_rating().getAggregate_rating()) < Double.parseDouble(o2.getRestaurant().getUser_rating().getAggregate_rating())) {
                    return -1;
                } else {
                    return 0;
                }
            });
            Toast.makeText(getActivity().getApplicationContext(), "Ascending order (feature : Rating)", Toast.LENGTH_SHORT).show();
        }

        ProgressBar progressBar = root.findViewById(R.id.progressBarHomeFrag);
        NestedScrollView nestedScrollView = root.findViewById(R.id.recycler_view_home_fragment);

        linearLayout = root.findViewById(R.id.linear_layout_for_restaurant_cards);
        linearLayout.removeAllViews();

        // send the bestRatedRestaurant object to the next activity and there search for the resID
        for (BestRatedRestaurant bestRatedRestaurant : bestRatedRestaurants) {
            linearLayout = root.findViewById(R.id.linear_layout_for_restaurant_cards);
            Restaurant restaurant = bestRatedRestaurant.getRestaurant();
            View restaurantCard = getLayoutInflater().inflate(R.layout.restaurant_card, null, false);

            TempRestaurant tempRestaurant = new TempRestaurant(restaurant.getId(),
                    restaurant.getName(), Double.parseDouble(restaurant.getUser_rating().getAggregate_rating()),
                    restaurant.getFeatured_image(), restaurant.getCuisines());

            // Adding ID to the views to recognise which restaurant card was clicked
            restaurantCard.setId(Integer.valueOf(restaurant.getId()));
            RestaurantClickListener onRestaurantClickListener = new RestaurantClickListener(tempRestaurant);
            restaurantCard.setOnClickListener(onRestaurantClickListener);

            TextView restaurantName = restaurantCard.findViewById(R.id.restaurantName);
            TextView cuisineType = restaurantCard.findViewById(R.id.cuisineType);
            TextView rating = restaurantCard.findViewById(R.id.rating);
            ImageView imageView = restaurantCard.findViewById(R.id.restaurantImage);
            String imgURL = restaurant.getFeatured_image();
            if (imgURL.equals("")) {
                imgURL = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/480px-No_image_available.svg.png";
            } else {
                imgURL = imgURL;
            }
            Picasso.get()
                    .load(imgURL)
                    .into(imageView);
            restaurantName.setText(restaurant.getName());
            cuisineType.setText(restaurant.getCuisines());
            rating.setText(restaurant.getUser_rating().getAggregate_rating() + " / 5");
            linearLayout.addView(restaurantCard);
        }

        progressBar.setVisibility(View.GONE);
        nestedScrollView.setVisibility(View.VISIBLE);
    }

    public class RestaurantClickListener implements View.OnClickListener{

        TempRestaurant restaurant;
        public RestaurantClickListener(TempRestaurant restaurant){
            this.restaurant = restaurant;
        }

        @Override
        public void onClick(View v) {
            int resId = v.getId();
            Intent intent = new Intent(HomeFragment.this.getActivity(), RestaurantMenuActivity.class);
            intent.putExtra("restaurantId", resId);
            intent.putExtra("restaurant", (Serializable) restaurant);
            startActivity(intent);
        }
    }

//    View.OnClickListener onClickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            int resId = v.getId();
//            Intent intent = new Intent(HomeFragment.this.getActivity(), RestaurantMenuActivity.class);
//            intent.putExtra("restaurantId", resId);
//            startActivity(intent);
//        }
//    };

    public void callGeoCode(double lat, double lng) {
        Map<String, Double> data = new HashMap<>();
        data.put("lat", lat);
        data.put("lon", lng);

        Log.e("callGeoCode", HomeScreen.zomatoAPI + " ");
        Log.e("callGeoCode", HomeScreen.lat + " " + HomeScreen.lng);
        Call<POJOGeoCodeResponseBody> call = HomeScreen.zomatoAPI.getGeoCode(data);
        call.enqueue(new Callback<POJOGeoCodeResponseBody>() {
            @Override
            public void onResponse(Call<POJOGeoCodeResponseBody> call, Response<POJOGeoCodeResponseBody> response) {
                //Main Response Object
                POJOGeoCodeResponseBody pojoGeoCodeResponseBody = response.body();

                //1.Location
                Location location = pojoGeoCodeResponseBody.getLocation();
                //2.Popularity
                Popularity popularity = pojoGeoCodeResponseBody.getPopularity();
                //3.Link
                String link = pojoGeoCodeResponseBody.getLink();
                //4.Nearby Restaurants
                List<NearbyRestaurant> nearbyRestaurantList = pojoGeoCodeResponseBody.getNearby_restaurants();
                new GetLocationDetailsTask().execute(location);
            }

            @Override
            public void onFailure(Call<POJOGeoCodeResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
                startActivity(getActivity().getIntent());
            }
        });
    }


    private class GetGeoCodeTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            Log.e("getGeoCodeTask", "inside geocodeTask");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            progressBar.setVisibility(View.VISIBLE);
            nestedScrollView.setVisibility(View.GONE);
            callGeoCode(HomeScreen.lat, HomeScreen.lng);
            return null;
        }
    }

    private class GetLocationDetailsTask extends AsyncTask<Location, Void, Void> {

        Location location;

        @Override
        protected void onPreExecute() {
            Log.e("getLocDetailsTask", "inside locationDetailsTask");
        }

        @Override
        protected Void doInBackground(Location... locations) {
            location = locations[0];
            callLocationDetails(location.getEntity_id(), location.getEntity_type());
            return null;
        }
    }


}