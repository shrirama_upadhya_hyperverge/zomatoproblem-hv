package com.example.zomatoproblem.ui.dashboard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.zomatoproblem.MainActivity;
import com.example.zomatoproblem.R;
import com.example.zomatoproblem.apis.models.RegisteredUser;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import static android.content.Context.MODE_PRIVATE;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private static final String SHARED_PREF_NAME = "panNo";
    private static final String KEY_NAME = "key_panNo";
    private static final String SHARED_PREF_LOGIN = "logIn";
    private static final String KEY_LOGIN = "key_logIn";
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        MaterialButton logoutButton = root.findViewById(R.id.logoutButton);
        ProgressBar progressBar = root.findViewById(R.id.progressBarDashboardFrag);
        MaterialCardView materialCardView = root.findViewById(R.id.dashboardCard);
        TextView userName = root.findViewById(R.id.userName);
        ImageView userImage = root.findViewById(R.id.userDP);
        TextView editDP = root.findViewById(R.id.editProfile);
        progressBar.setVisibility(View.VISIBLE);
        editDP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfileImage();
            }
        });

        TextView panNumber = root.findViewById(R.id.userPANNumber);
        SharedPreferences sp = getActivity().getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        String pan = sp.getString(KEY_NAME, null);
        panNumber.setText("PAN Number : " + pan);
        DocumentReference docRef = db.collection("RegisteredUsers").document(pan);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                RegisteredUser registeredUser = documentSnapshot.toObject(RegisteredUser.class);
                userName.setText(registeredUser.getName());
                Picasso.get()
                        .load(registeredUser.getImage())
                        .into(userImage);
                progressBar.setVisibility(View.GONE);
                materialCardView.setVisibility(View.VISIBLE);
            }
        }).addOnFailureListener(e -> {
            Log.e("onFailList", e.getMessage());
            progressBar.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Failure : " + e.getMessage(), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        });

        logoutButton.setOnClickListener(v -> {
            SharedPreferences sp2 = getActivity().getSharedPreferences(SHARED_PREF_LOGIN, MODE_PRIVATE);
            SharedPreferences.Editor e2 = sp2.edit();
            e2.putBoolean(KEY_LOGIN, false);
            e2.apply();
            Toast.makeText(getActivity(), "Logout Successful", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        });
        return root;
    }

    private void editProfileImage() {
        Toast.makeText(getActivity(), "This feature is almost done, just a few more additions required! Will finsh it soon", Toast.LENGTH_SHORT).show();
    }
}