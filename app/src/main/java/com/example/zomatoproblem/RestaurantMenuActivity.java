package com.example.zomatoproblem;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

public class RestaurantMenuActivity extends AppCompatActivity {

    private int restaurantId;
    private TempRestaurant restaurant;
    private ImageView restuarantImageView;
    private TextView restaurantNameTextView, ratingTextView, cusineTypeTextView, orderTextView;
    private LinearLayout linearLayout;
    private ImageButton nextImageButton;
    private MaterialCardView orderCardView;

    private ArrayList<Dish> orderedItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.add_restaurant_toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        restaurant = (TempRestaurant) getIntent().getSerializableExtra("restaurant");
        restaurantId = getIntent().getIntExtra("restaurantId",1);
        restuarantImageView = findViewById(R.id.restaurantImage);
        restaurantNameTextView = findViewById(R.id.restaurant_name_text_view);
        ratingTextView = findViewById(R.id.rating_text_view);
        cusineTypeTextView = findViewById(R.id.cuisine_type_text_view);
        linearLayout = findViewById(R.id.linear_layout_for_dish_cards);
        orderTextView = findViewById(R.id.order_text_view);
        nextImageButton = findViewById(R.id.next_image_button);
        orderCardView = findViewById(R.id.order_card_view);

        CollapsingToolbarLayout toolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_tool_bar);
        toolBarLayout.setTitle("");

        restaurantNameTextView.setText(restaurant.getRestaurantName());
        ratingTextView.setText(String.valueOf(restaurant.getRating()));
        cusineTypeTextView.setText(restaurant.getCuisineType());

        String image_url = restaurant.getFeaturedImage();
        if (image_url.equals("")) {
            image_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/480px-No_image_available.svg.png";
        } else {
            image_url = image_url;
        }
        Picasso.get()
                .load(image_url)
                .into(restuarantImageView);

        orderedItems = new ArrayList<Dish>();

        // Since menu data is not available through Zomato API, we've taken fake data
        ArrayList<Dish> dishList = new ArrayList<Dish>();
        dishList.add(new Dish("Paneer Butter Masala",true,289));
        dishList.add(new Dish("Dal Makhani",true,239));
        dishList.add(new Dish("Mutton Curry",false,675));
        dishList.add(new Dish("Butter Chicken",false,369));
        dishList.add(new Dish("Chicken Rara",false,289));
        dishList.add(new Dish("Kadhai Paneer",true,279));
        dishList.add(new Dish("Chatpate Aloo",true,229));
        dishList.add(new Dish("Paneer Tikka",true,349));
        dishList.add(new Dish("Dal Fry",true,239));
        dishList.add(new Dish("Malai Kofta",true,279));
        dishList.add(new Dish("Palak Paneer",true,279));
        dishList.add(new Dish("Rajma",true,229));
        dishList.add(new Dish("Kadhai Chaap",true,279));
        dishList.add(new Dish("Veg Thali",true,329));
        dishList.add(new Dish("Non Veg Thali",false,369));

        int i = 0;

        for(Dish dish: dishList){
            View dishCard = getLayoutInflater().inflate(R.layout.dish_card, null, false);

            String dishName = dish.getDishName();
            boolean isVeg = dish.getVeg();
            int price = dish.getPrice();

            TextView nameTextView = dishCard.findViewById(R.id.dish_name_text_view);
            ImageView vegImageView = dishCard.findViewById(R.id.veg_icon_image_view);
            TextView priceTextView = dishCard.findViewById(R.id.price_text_view);
            CardView addButton = dishCard.findViewById(R.id.add_to_cart_button);

            AddDishClickListener onAddDishClickListener = new AddDishClickListener(dish);
            addButton.setOnClickListener(onAddDishClickListener);

            nameTextView.setText(dishName);
            if(isVeg == false){
                vegImageView.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_non_veg));
            }
            priceTextView.setText("₹" + price);
            addButton.setId(i++);
            linearLayout.addView(dishCard);
        }


        nextImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(RestaurantMenuActivity.this, OrderSummaryActivity.class);
//                intent.putExtra("orderedItems", orderedItems);
//                startActivity(intent);

                BottomSheetDialog bottomSheet = new BottomSheetDialog();
                Bundle bundle = new Bundle();
                bundle.putSerializable("order", (Serializable) orderedItems);
                bundle.putString("restaurant_name",restaurant.getRestaurantName());
                bottomSheet.setArguments(bundle);
                bottomSheet.show(getSupportFragmentManager(),"ModalBottomSheet");
            }
        });


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    public class AddDishClickListener implements View.OnClickListener{

        Dish dish;
        public AddDishClickListener(Dish dish){
            this.dish = dish;
        }

        @Override
        public void onClick(View v) {
            orderCardView.setVisibility(View.VISIBLE);
            String orderString = "";
            for(Dish item: orderedItems){
                orderString = orderString + item.getDishName() + ", ";
            }
            orderedItems.add(dish);
            orderString = orderString + (orderedItems.get(orderedItems.size()-1).getDishName());
            orderTextView.setText(orderString);
        }
    }
}