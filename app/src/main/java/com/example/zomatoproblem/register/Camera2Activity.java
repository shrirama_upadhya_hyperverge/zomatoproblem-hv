package com.example.zomatoproblem.register;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.zomatoproblem.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class Camera2Activity extends AppCompatActivity {

    public static Camera2Activity camera2Activity;
    final int CAMERA_REQUEST_CODE = 1;
    private CameraManager cameraManager;
    private int cameraFacing;
    private TextureView.SurfaceTextureListener surfaceTextureListener;
    private String cameraId;
    private HandlerThread backgroundThread;
    private Size previewSize;
    private Handler backgroundHandler;
    private CameraDevice.StateCallback stateCallback;
    private CameraDevice cameraDevice;
    private TextureView textureView;
    private CameraCaptureSession cameraCaptureSession;
    private CaptureRequest.Builder captureRequestBuilder;
    private CaptureRequest captureRequest;
    private FloatingActionButton captureButton, flipCameraButton;
    private TextView textViewRegister;
    private ProgressBar progressBar;
    private ImageView textureViewImage;
    private final String panCARD = "Please capture your PANCARD image";
    private final String selfie = "Please take a selfie";
    private String stringFromReviewScreen;
    private String panNo = "Not initialised", name = "Not initialised";
    private Boolean isSelfie = false;

    public static Camera2Activity getInstance(){
        return camera2Activity;
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        camera2Activity = this;

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, CAMERA_REQUEST_CODE);

        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        cameraFacing = CameraCharacteristics.LENS_FACING_BACK;

        textureView = (TextureView) findViewById(R.id.textureViewRegister);
        textureViewImage = findViewById(R.id.textureViewImage);
        captureButton =(FloatingActionButton) findViewById(R.id.captureRegister);
        flipCameraButton = (FloatingActionButton) findViewById(R.id.flipCameraRegister);
        progressBar = findViewById(R.id.progressBarRegistrationCapture);
        textViewRegister = findViewById(R.id.textViewRegister);
        textureViewImage.setVisibility(View.GONE);
        textureView.setVisibility(View.VISIBLE);

        textViewRegister.setText(panCARD);
        stringFromReviewScreen = getIntent().getStringExtra("imageCategory");
        panNo = getIntent().getStringExtra("panNo");
        name = getIntent().getStringExtra("name");
        if(stringFromReviewScreen!=null && stringFromReviewScreen.equals("selfie")){
            isSelfie = true;
            cameraFacing = CameraCharacteristics.LENS_FACING_FRONT;
            textViewRegister.setText(selfie);
        }

        flipCameraButton.setOnClickListener(v->{
            if(cameraFacing == (CameraCharacteristics.LENS_FACING_BACK)){
                cameraFacing = CameraCharacteristics.LENS_FACING_FRONT;
            }
            else{
                cameraFacing = CameraCharacteristics.LENS_FACING_BACK;
            }
            setUpCamera();
            openCamera();
        });

        surfaceTextureListener = new TextureView.SurfaceTextureListener() {

            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
                setUpCamera();
                openCamera();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

            }
        };


        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ProcessImageTask().execute();
            }
        });


        stateCallback = new CameraDevice.StateCallback() {
            @Override
            public void onOpened(CameraDevice cameraDevice) {
                Camera2Activity.this.cameraDevice = cameraDevice;
                createPreviewSession();
            }

            @Override
            public void onDisconnected(CameraDevice cameraDevice) {
                cameraDevice.close();
                Camera2Activity.this.cameraDevice = null;
            }

            @Override
            public void onError(CameraDevice cameraDevice, int error) {
                cameraDevice.close();
                Camera2Activity.this.cameraDevice = null;
            }
        };
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setUpCamera() {
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cameraCharacteristics =
                        cameraManager.getCameraCharacteristics(cameraId);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) ==
                        cameraFacing) {
                    StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(
                            CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                    previewSize = streamConfigurationMap.getOutputSizes(SurfaceTexture.class)[0];
                    this.cameraId = cameraId;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    private void openCamera() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                cameraManager.openCamera(cameraId, stateCallback, backgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void openBackgroundThread() {
        backgroundThread = new HandlerThread("camera_background_thread");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    private void closeCamera() throws IOException {
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }

        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    private void closeBackgroundThread() {
        if (backgroundHandler != null) {
            backgroundThread.quitSafely();
            backgroundThread = null;
            backgroundHandler = null;
        }
    }


    private void lock() {
        try {
            cameraCaptureSession.capture(captureRequestBuilder.build(),
                    null, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void unlock() {
        try {
            cameraCaptureSession.setRepeatingRequest(captureRequestBuilder.build(),
                    null, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void createPreviewSession() {
        try {
            SurfaceTexture surfaceTexture = textureView.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(previewSurface);

            cameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                            if (cameraDevice == null) {
                                return;
                            }

                            try {
                                captureRequest = captureRequestBuilder.build();
                                Camera2Activity.this.cameraCaptureSession = cameraCaptureSession;
                                Camera2Activity.this.cameraCaptureSession.setRepeatingRequest(captureRequest,
                                        null, backgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                            startActivity(getIntent());
                        }
                    }, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        openBackgroundThread();
        if (textureView.isAvailable()) {
            setUpCamera();
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(surfaceTextureListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            closeCamera();
        } catch (IOException e) {
            e.printStackTrace();
        }
        closeBackgroundThread();
    }

    private class ProcessImageTask extends AsyncTask<Void,Void,Void> {

        File pictureFile;

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            captureButton.setVisibility(View.GONE);
            flipCameraButton.setVisibility(View.GONE);
            Bitmap textureImage = textureView.getBitmap();
            textureViewImage.setImageBitmap(textureImage);
            textureViewImage.setVisibility(View.VISIBLE);
            lock();
        }

        @Override
        protected Void doInBackground(Void... voids) {


            File pictureFileDir = new File(getCacheDir()+File.separator+"images");
            Log.d("path",""+pictureFileDir);

            pictureFileDir.mkdirs();
            String photoFile;
            if(isSelfie){
                photoFile = "selfie.jpg";
            }else{
                photoFile = "PANCard.jpg";
            }
            String filename = pictureFileDir.getPath() + File.separator + photoFile;

            pictureFile = new File(filename);
            Log.d("path",""+pictureFileDir);

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                textureView.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
                //                        Toast.makeText(MainActivity.this, "New Image saved:" + photoFile,
                //                                Toast.LENGTH_LONG).show();
            } catch (Exception error) {
                Log.d("tag", "File" + filename + "not saved: "
                        + error.getMessage());
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(Camera2Activity.this, "Image could not be saved.",
                                Toast.LENGTH_LONG).show();
                    }
                });

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent intent = new Intent(Camera2Activity.this, ReviewRegister.class);
            intent.putExtra("imagePath", pictureFile.getPath());
            intent.putExtra("version", 2);
            if(isSelfie){
                intent.putExtra("isSelfie", true);
                intent.putExtra("panNo", panNo);
                intent.putExtra("name", name);
            }
            textureViewImage.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            captureButton.setVisibility(View.VISIBLE);
            flipCameraButton.setVisibility(View.VISIBLE);
            startActivity(intent);
            unlock();
        }
    }
}

