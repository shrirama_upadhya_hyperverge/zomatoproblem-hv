package com.example.zomatoproblem.register;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zomatoproblem.HomeScreen;
import com.example.zomatoproblem.R;
import com.example.zomatoproblem.RegisterActivity;
import com.example.zomatoproblem.apis.api.HypervergeAPI;
import com.example.zomatoproblem.apis.models.CompareOutput;
import com.example.zomatoproblem.apis.models.ComparisonResult;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReviewRegister extends AppCompatActivity {
    private ImageView reviewImageView;
    private Button closeButton;
    private String image_path;
    private Bitmap rotatedBitmap;
    private int cameraApiVersion;
    private boolean isSelfie;
    private final String baseURLForComparison = "https://ind-faceid.hyperverge.co/v1/";
    private final String baseURLForKYC = "https://ind-docs.hyperverge.co/v2.0/";
    private HypervergeAPI hypervergeAPIForComparison, hypervergeAPIForKYC;

    private Intent intent;
    private String name = "NotInitialised", panNo = "NotInitialised";
    private boolean matched = false;
    private boolean noFace = false;
    private OkHttpClient client;
    private final StorageReference storageRef = FirebaseStorage.getInstance().getReference("Images");
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ProgressBar progressBar;
    private static final String SHARED_PREF_NAME = "panNo";
    private static final String KEY_NAME = "key_panNo";
    private static final String SHARED_PREF_LOGIN = "logIn";
    private static final String KEY_LOGIN = "key_logIn";

    public static ReviewRegister reviewRegister;
    public static ReviewRegister getInstance(){
        return reviewRegister;
    }
    public ReviewRegister() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reviewRegister = this;
        /*View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/
        setContentView(R.layout.activity_review_register);
        reviewImageView = findViewById(R.id.review_image_view);
        closeButton = findViewById(R.id.completeRegistration);
        progressBar = findViewById(R.id.progress_circular);

        closeButton.setVisibility(View.GONE);
        reviewImageView.setVisibility(View.GONE);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("appId", "857acb")
                        .header("appKey", "e0d888e703691bccbf92")
                        .header("Content-Type", "multipart/form-data")
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });
        client = httpClient.build();

        image_path = getIntent().getStringExtra("imagePath");
        cameraApiVersion = getIntent().getIntExtra("version", 1);
        isSelfie = getIntent().getBooleanExtra("isSelfie", false);

        panNo = getIntent().getStringExtra("panNo");
        name = getIntent().getStringExtra("name");

        if (!isSelfie) {
            closeButton.setText("Come on! Let's click a selfie!");
            Log.e("inside IF", panNo + "");
            readKYC();
        } else {
            progressBar.setVisibility(View.GONE);
            closeButton.setVisibility(View.VISIBLE);
            reviewImageView.setVisibility(View.VISIBLE);
        }

        Bitmap bitmap = BitmapFactory.decodeFile(image_path);
        if (cameraApiVersion == 1) {
            rotatedBitmap = rotate(bitmap);
        } else {
            rotatedBitmap = bitmap;
        }
        reviewImageView.setImageBitmap(rotatedBitmap);
        Paint mPaint = new Paint();
        mPaint.setStrokeWidth(5);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        Bitmap tempBitmap = Bitmap.createBitmap(rotatedBitmap.getWidth(),
                rotatedBitmap.getHeight(),
                Bitmap.Config.RGB_565);
        Canvas tempCanvas = new Canvas(tempBitmap);
        tempCanvas.drawBitmap(rotatedBitmap, 0, 0, null);

        FaceDetector faceDetector = new FaceDetector.Builder(getApplicationContext())
                .setTrackingEnabled(false)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .build();
        if (!faceDetector.isOperational()) {
            new AlertDialog.Builder(getApplicationContext()).setMessage("Could not set up faceDetector").show();
            return;
        }
        Frame frame = new Frame.Builder().setBitmap(rotatedBitmap).build();
        SparseArray<Face> faces = faceDetector.detect(frame);
        if (faces.size() <= 0) {
            noFace = true;
            Toast.makeText(ReviewRegister.this,
                    "No faces detected! Please make sure you don't have anything covering the face or please go closer so that the face can be detected",
                    Toast.LENGTH_SHORT)
                    .show();
            onBackPressed();
        }
        for (int i = 0; i < faces.size(); ++i) {
            Face thisFace = faces.valueAt(i);
            float x1 = thisFace.getPosition().x;
            float y1 = thisFace.getPosition().y;
            float x2 = x1 + thisFace.getWidth();
            float y2 = y1 + thisFace.getHeight();
            tempCanvas.drawRoundRect(new RectF(x1, y1, x2, y2), 2, 2, mPaint);
        }
        reviewImageView.setImageDrawable(new BitmapDrawable(getResources(), tempBitmap));
        reviewImageView.setVisibility(View.VISIBLE);
        faceDetector.release();
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSelfie) {
                    compareImages();
                } else {
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void readKYC() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseURLForKYC)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        hypervergeAPIForKYC = retrofit.create(HypervergeAPI.class);
        Uri panID = Uri.fromFile(new File(getCacheDir() + File.separator + "images" + File.separator + "PANCard.jpg"));
        File idFile = new File(panID.getPath());
        RequestBody idRequestFile = RequestBody.create(MediaType.parse("image/*"),
                idFile);
        MultipartBody.Part idBody =
                MultipartBody.Part.createFormData("image", idFile.getName(), idRequestFile);
        Call<JsonObject> call = hypervergeAPIForKYC.getKYC(idBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (!response.isSuccessful()) {
                    Log.e("App", "Code : " + response.code() + " : " + response.message() + "\n Response Headers" + response.headers());
                    Toast.makeText(ReviewRegister.this, "KYC unsuccessful! Please make sure you capture proper PAN Card image", Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }
                JsonObject object = response.body();
                String status = object.get("status").getAsString();
                Log.e("onResponse", status);

                JsonArray result = (JsonArray) object.get("result");
                JsonObject result0 = result.get(0).getAsJsonObject();
                JsonObject details = (JsonObject) result0.get("details");
                JsonObject nameDetails = (JsonObject) details.get("name");
                name = nameDetails.get("value").getAsString();
                JsonObject panDetails = (JsonObject) details.get("pan_no");
                panNo = panDetails.get("value").getAsString();
                Log.e("onResponse", name + " : " + panNo);

                if(panNo==null || name == null){
                    Toast.makeText(ReviewRegister.this, "KYC unsuccessful! Please make sure you capture proper PAN Card image", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    finish();
                }

                if (cameraApiVersion == 1) {
                    intent = new Intent(ReviewRegister.this, CameraActivity.class);
                } else {
                    intent = new Intent(ReviewRegister.this, Camera2Activity.class);
                }
                intent.putExtra("imageCategory", "selfie");
                intent.putExtra("panNo", panNo);
                intent.putExtra("name", name);
                progressBar.setVisibility(View.GONE);
                closeButton.setVisibility(View.VISIBLE);
                reviewImageView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(ReviewRegister.this, "KYC unsuccessful! Please make sure you capture proper PAN Card image", Toast.LENGTH_SHORT).show();
                finish();
                Log.e("onFailure", t.getMessage());
            }
        });
    }

    private void compareImages() {
        progressBar.setVisibility(View.VISIBLE);
        closeButton.setVisibility(View.GONE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseURLForComparison)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        hypervergeAPIForComparison = retrofit.create(HypervergeAPI.class);
        Uri panID = Uri.fromFile(new File(getCacheDir() + File.separator + "images" + File.separator + "PANCard.jpg"));
        Uri selfie = Uri.fromFile(new File(getCacheDir() + File.separator + "images" + File.separator + "selfie.jpg"));
        Log.e("App", "" + panID);
        File selfieFile = new File(selfie.getPath());
        File idFile = new File(panID.getPath());
        RequestBody selfieRequestFile = RequestBody.create(MediaType.parse("image/*"),
                selfieFile);
        RequestBody idRequestFile = RequestBody.create(MediaType.parse("image/*"),
                idFile);
        MultipartBody.Part selfieBody =
                MultipartBody.Part.createFormData("selfie", selfieFile.getName(), selfieRequestFile);
        MultipartBody.Part idBody =
                MultipartBody.Part.createFormData("id", idFile.getName(), idRequestFile);
        Call<ComparisonResult> call = hypervergeAPIForComparison.compare(selfieBody, idBody);
        call.enqueue(new Callback<ComparisonResult>() {
            @Override
            public void onResponse(Call<ComparisonResult> call, Response<ComparisonResult> response) {
                if (!response.isSuccessful()) {
                    Log.e("App", "Code : " + response.code() + " : " + response.message() + "\n Response Headers" + response.headers());
                    return;
                }
                ComparisonResult comaprisonResult = response.body();
                String content = "";
                content += "Status : " + comaprisonResult.getStatus() + "\n";
                content += "StatusCode : " + comaprisonResult.getStatusCode() + "\n";
                CompareOutput output = comaprisonResult.getResult();
                content += "Conf : " + output.getConf() + "\n";
                content += "Match : " + output.getMatch() + "\n";
                content += "Score : " + output.getScore() + "\n";
                content += "To-be-reviewed : " + output.getToReview() + "\n";
                Log.e("App", content);
                if (output.getMatch().equalsIgnoreCase("yes")) {
                    matched = true;
                }
                if (matched) {
                    Log.e("afterMatched", "" + panNo);
                    saveInFirebase();
                    Toast.makeText(ReviewRegister.this, "Registration successful", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ReviewRegister.this, HomeScreen.class));
                    finish();
                } else {
                    Toast.makeText(ReviewRegister.this, "PANCARD image and Selfie don't match, Please try again", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ReviewRegister.this, RegisterActivity.class));
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ComparisonResult> call, Throwable t) {
                Log.e("App", t.getMessage());
            }
        });


    }

    private void saveInFirebase() {
        Log.e("saveInFirebase", "" + panNo);
        StorageReference ref = storageRef.child(panNo);
        Uri selfie = Uri.fromFile(new File(getCacheDir() + File.separator + "images" + File.separator + "selfie.jpg"));

        UploadTask uploadTask = ref.putFile(selfie);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                // Continue with the task to get the download URL
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    Log.e("afterComplete", downloadUri + "");
                    Map<String, Object> user = new HashMap<>();
                    user.put("Name", name);
                    user.put("ID", panNo);
                    user.put("Image", downloadUri.toString());
                    db.collection("RegisteredUsers").document(panNo).set(user)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    SharedPreferences sp = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
                                    SharedPreferences.Editor e = sp.edit();
                                    e.putString(KEY_NAME, panNo);
                                    e.apply();
                                    SharedPreferences sp2 = getSharedPreferences(SHARED_PREF_LOGIN, MODE_PRIVATE);
                                    SharedPreferences.Editor e2 = sp2.edit();
                                    e2.putBoolean(KEY_LOGIN, true);
                                    e2.apply();
                                    Log.e("onSuccessList", "data added successfully");
                                    Toast.makeText(ReviewRegister.this, "data added successfully to firebase", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.e("onFailList", "data failed");
                                }
                            });
                } else {
                    // Handle failures
                    // ...
                    Log.e("onComplete", "Uploading image failed");
                }
            }
        });
    }

    private Bitmap rotate(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.setRotate(90);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
    }
}

