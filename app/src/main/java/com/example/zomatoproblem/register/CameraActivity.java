package com.example.zomatoproblem.register;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.zomatoproblem.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class CameraActivity extends AppCompatActivity implements MediaRecorder.OnInfoListener, MediaRecorder.OnErrorListener {

    public static Bitmap bitmap;
    public static CameraActivity cameraActivity;
    private final String panCARD = "Please capture your PANCARD image";
    private final String selfie = "Please take a selfie";
    ProgressBar progressBar;
    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private Context myContext;
    private LinearLayout cameraPreview;
    private FloatingActionButton captureButton, flipCameraButton;
    private boolean cameraFront = false;
    private TextView textViewRegister;
    private String stringFromReviewScreen;
    private String panNo = "Not initialised", name = "Not initialised";
    private Boolean isSelfie = false;

    private MediaRecorder mediaRecorder;
    private Camera camera;

    public static CameraActivity getInstance(){
        return cameraActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        cameraActivity = this;

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;

        cameraPreview = (LinearLayout) findViewById(R.id.preview_linear_layout_register);
        captureButton = (FloatingActionButton) findViewById(R.id.captureRegister);
        flipCameraButton = (FloatingActionButton) findViewById(R.id.flipCameraRegister);
        progressBar = findViewById(R.id.progressBarRegistrationCapture);
        textViewRegister = findViewById(R.id.textViewRegister);
        cameraPreview.setVisibility(View.VISIBLE);

        textViewRegister.setText(panCARD);
        stringFromReviewScreen = getIntent().getStringExtra("imageCategory");
        panNo = getIntent().getStringExtra("panNo");
        name = getIntent().getStringExtra("name");
        if (stringFromReviewScreen != null && stringFromReviewScreen.equals("selfie")) {
            isSelfie = true;
            cameraFront = true;
            textViewRegister.setText(selfie);
        }

        mCamera = Camera.open();
        mCamera.setDisplayOrientation(90);

        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.takePicture(null, null, mPicture);
            }
        });

        flipCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the number of cameras
                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {
                    //release the old camera instance
                    //switch camera, from the front and the back and vice versa
                    releaseCamera();
                    chooseCamera();
                }
            }
        });

        mCamera.startPreview();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mPreview.getHolder().getSurface()!= null){
                    Log.e("app", "starting recording session");
                    startRecording();
                }else{
                    Log.e("app", "no holder surface");
                }
            }
        }, 3000);



    }

    private void startRecording() {
        if (prepareVideoRecorder()) {
            // Camera is available and unlocked, MediaRecorder is prepared,
            // now you can start recording
            mediaRecorder.start();

            // inform the user that recording has started
            Log.e("App", "recording has started");
        } else {
            // prepare didn't work, release the camera
            releaseMediaRecorder();
            Log.e("App", "preparing didn't work");
            // inform user
        }
    }

    private void stopRecording() {
        mediaRecorder.stop();  // stop the recording
        releaseMediaRecorder(); // release the MediaRecorder object
        mCamera.lock();         // take camera access back from MediaRecorder

        // inform the user that recording has stopped
        Log.e("app", "recording has stopped");
    }

    private boolean prepareVideoRecorder(){

        camera = getCameraInstance();
        mediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        camera.unlock();
        mediaRecorder.setCamera(camera);
        //Rotation problem solved here
        if(cameraFront){
            mediaRecorder.setOrientationHint(270);
        }
        else{
            mediaRecorder.setOrientationHint(90);
        }
        mediaRecorder.setMaxDuration(5000); // 5s
//        mediaRecorder.setMaxFileSize(1000000); // 2MB
        mediaRecorder.setOnErrorListener(this);
        mediaRecorder.setOnInfoListener(this);

        // Step 2: Set sources
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_480P));

        // Step 4: Set output file
        mediaRecorder.setOutputFile(getOutputMediaFile(MEDIA_TYPE_VIDEO).toString());

        // Step 5: Set the preview output
        mediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        // Step 6: Prepare configured MediaRecorder
        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d("App", "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d("App", "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }


    public Camera getCameraInstance() {
        return this.mCamera;
    }

    private int findFrontFacingCamera() {

        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;

    }


    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;

            }

        }
        return cameraId;
    }


    public void onResume() {

        super.onResume();
        if (mCamera == null) {
            mCamera = Camera.open();
            mCamera.setDisplayOrientation(90);
            mPicture = getPictureCallback();
            mPreview.refreshCamera(mCamera);
            Log.d("nu", "null");
        } else {
            Log.d("nu", "no null");
        }

    }


    public void chooseCamera() {
        //if the camera preview is the front
        if (cameraFront) {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview

                mCamera = Camera.open(cameraId);
                mCamera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the frontFacingCamera
                //set a picture callback
                //refresh the preview
                mCamera = Camera.open(cameraId);
                mCamera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        //when on Pause, release camera in order to be used from other applications
        releaseMediaRecorder();
        releaseCamera();
    }


    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                new ProcessImageTask().execute(data);
            }
        };
        return picture;
    }

    @Override
    public void onError(MediaRecorder mr, int what, int extra) {
        if(what == MediaRecorder.MEDIA_ERROR_SERVER_DIED){
            Log.e("MediaRecorder", "server died error");

        }
        else if(what == MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN){
            Log.e("MediaRecorder", "unknown error");
        }
        stopRecording();
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        if(what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED){
            Log.e("MediaRecorder", "max duration reached");
            stopRecording();
        }
        if(what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED){
            Log.e("MediaRecorder", "max file size reached");
            stopRecording();
        }
    }

    private class ProcessImageTask extends AsyncTask<byte[], Void, Void> {

        File pictureFile;

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            captureButton.setVisibility(View.GONE);
            flipCameraButton.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(byte[]... bytes) {
            byte[] data = bytes[0];
            File pictureFileDir = new File(getCacheDir() + File.separator + "images");
            Log.d("path", "" + pictureFileDir);

            pictureFileDir.mkdirs();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
            String date = dateFormat.format(new Date());
            String photoFile = "Picture_" + date + ".jpg";

            String filename = pictureFileDir.getPath() + File.separator + photoFile;

            pictureFile = new File(filename);
            Log.d("path", "" + pictureFileDir);

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (Exception error) {
                Log.d("tag", "File" + filename + "not saved: "
                        + error.getMessage());
                Toast.makeText(CameraActivity.this, "Image could not be saved.", Toast.LENGTH_LONG).show();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent intent = new Intent(CameraActivity.this, ReviewRegister.class);
            intent.putExtra("imagePath", pictureFile.getPath());
            intent.putExtra("version", 1);
            if (isSelfie) {
                intent.putExtra("isSelfie", true);
                intent.putExtra("panNo", panNo);
                intent.putExtra("name", name);
            }
            progressBar.setVisibility(View.GONE);
            captureButton.setVisibility(View.VISIBLE);
            flipCameraButton.setVisibility(View.VISIBLE);
            startActivity(intent);

        }
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }
}
