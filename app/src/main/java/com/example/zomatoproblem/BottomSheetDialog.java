package com.example.zomatoproblem;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.Date;

public class BottomSheetDialog extends BottomSheetDialogFragment {

    private static String userId;
    private ArrayList<Dish> orderedItems;
    private LinearLayout linearLayout;
    private int totalPrice = 0;
    private TextView grandTotalTextView;
    private Button finishOrderButton;
    private String restaurantName;
    private String orderedItemsString;

    private OrderDatabase mDb;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_layout,
                container, false);

        orderedItems = (ArrayList<Dish>) getArguments().getSerializable("order");
        restaurantName = getArguments().getString("restaurant_name");
        linearLayout = v.findViewById(R.id.linear_layout_for_order);
        grandTotalTextView = v.findViewById(R.id.total_text_view);
        finishOrderButton = v.findViewById(R.id.finish_order_button);

        orderedItemsString = "";

        for(Dish item: orderedItems){
            View dishItem = getLayoutInflater().inflate(R.layout.order_item, null, false);

            TextView dishNameTextView = dishItem.findViewById(R.id.order_dish_name);
            TextView dishPriceTextView = dishItem.findViewById(R.id.order_dish_price);

            dishNameTextView.setText(item.getDishName());
            dishPriceTextView.setText("₹" + item.getPrice());

            totalPrice += item.getPrice();
            orderedItemsString += (item.getDishName() + "\n");

            linearLayout.addView(dishItem);
        }

        mDb = OrderDatabase.getInstance(getContext());

        grandTotalTextView.setText("₹" + totalPrice);

        finishOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Date d = new Date();
                // Creating an object of model 'Order' to insert it in the Room DB
                final Order order = new Order(restaurantName,orderedItemsString,d,totalPrice);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.orderDao().insertOrder(order);
                    }
                });

                Toast.makeText(getActivity(), "Order Placed", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(),OrderPlacedActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }
}
