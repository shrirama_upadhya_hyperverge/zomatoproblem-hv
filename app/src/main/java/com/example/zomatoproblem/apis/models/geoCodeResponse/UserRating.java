package com.example.zomatoproblem.apis.models.geoCodeResponse;

public class UserRating{
    public String aggregate_rating;
    public String rating_text;
    public String rating_color;
    public com.example.zomatoproblem.apis.models.geoCodeResponse.RatingObj rating_obj;
    public int votes;

    public String getAggregate_rating() {
        return aggregate_rating;
    }

    public String getRating_text() {
        return rating_text;
    }

    public String getRating_color() {
        return rating_color;
    }

    public com.example.zomatoproblem.apis.models.geoCodeResponse.RatingObj getRating_obj() {
        return rating_obj;
    }

    public int getVotes() {
        return votes;
    }
}
