package com.example.zomatoproblem.apis.models.geoCodeResponse;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Restaurant{
    @JsonProperty("R")
    public com.example.zomatoproblem.apis.models.geoCodeResponse.R r;
    public String apikey;
    public String id;
    public String name;
    public String url;
    public Location location;
    public int switch_to_order_menu;
    public String cuisines;
    public int average_cost_for_two;
    public int price_range;
    public String currency;
    public List<Object> offers;
    public int opentable_support;
    public int is_zomato_book_res;
    public String mezzo_provider;
    public int is_book_form_web_view;
    public String book_form_web_view_url;
    public String book_again_url;
    public String thumb;
    public UserRating user_rating;
    public String photos_url;
    public String menu_url;
    public String featured_image;
    public Object medio_provider;
    public int has_online_delivery;
    public int is_delivering_now;
    public String store_type;
    public boolean include_bogo_offers;
    public String deeplink;
    public int is_table_reservation_supported;
    public int has_table_booking;
    public String events_url;
    public String order_url;
    public String order_deeplink;
    public String book_url;
    public List<com.example.zomatoproblem.apis.models.geoCodeResponse.ZomatoEvent> zomato_events;

    public com.example.zomatoproblem.apis.models.geoCodeResponse.R getR() {
        return r;
    }

    public String getApikey() {
        return apikey;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public Location getLocation() {
        return location;
    }

    public int getSwitch_to_order_menu() {
        return switch_to_order_menu;
    }

    public String getCuisines() {
        return cuisines;
    }

    public int getAverage_cost_for_two() {
        return average_cost_for_two;
    }

    public int getPrice_range() {
        return price_range;
    }

    public String getCurrency() {
        return currency;
    }

    public List<Object> getOffers() {
        return offers;
    }

    public int getOpentable_support() {
        return opentable_support;
    }

    public int getIs_zomato_book_res() {
        return is_zomato_book_res;
    }

    public String getMezzo_provider() {
        return mezzo_provider;
    }

    public int getIs_book_form_web_view() {
        return is_book_form_web_view;
    }

    public String getBook_form_web_view_url() {
        return book_form_web_view_url;
    }

    public String getBook_again_url() {
        return book_again_url;
    }

    public String getThumb() {
        return thumb;
    }

    public UserRating getUser_rating() {
        return user_rating;
    }

    public String getPhotos_url() {
        return photos_url;
    }

    public String getMenu_url() {
        return menu_url;
    }

    public String getFeatured_image() {
        return featured_image;
    }

    public Object getMedio_provider() {
        return medio_provider;
    }

    public int getHas_online_delivery() {
        return has_online_delivery;
    }

    public int getIs_delivering_now() {
        return is_delivering_now;
    }

    public String getStore_type() {
        return store_type;
    }

    public boolean isInclude_bogo_offers() {
        return include_bogo_offers;
    }

    public String getDeeplink() {
        return deeplink;
    }

    public int getIs_table_reservation_supported() {
        return is_table_reservation_supported;
    }

    public int getHas_table_booking() {
        return has_table_booking;
    }

    public String getEvents_url() {
        return events_url;
    }

    public String getOrder_url() {
        return order_url;
    }

    public String getOrder_deeplink() {
        return order_deeplink;
    }

    public String getBook_url() {
        return book_url;
    }

    public List<com.example.zomatoproblem.apis.models.geoCodeResponse.ZomatoEvent> getZomato_events() {
        return zomato_events;
    }
}
