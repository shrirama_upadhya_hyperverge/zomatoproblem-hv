package com.example.zomatoproblem.apis.models.geoCodeResponse;

import java.util.List;

public class Event{
    public int event_id;
    public String friendly_start_date;
    public String friendly_end_date;
    public String friendly_timing_str;
    public String start_date;
    public String end_date;
    public String end_time;
    public String start_time;
    public int is_active;
    public String date_added;
    public List<Photo> photos;
    public List<Object> restaurants;
    public int is_valid;
    public String share_url;
    public int show_share_url;
    public String title;
    public String description;
    public String display_time;
    public String display_date;
    public int is_end_time_set;
    public String disclaimer;
    public int event_category;
    public String event_category_name;
    public String book_link;
    public List<Type> types;
    public ShareData share_data;

    public int getEvent_id() {
        return event_id;
    }

    public String getFriendly_start_date() {
        return friendly_start_date;
    }

    public String getFriendly_end_date() {
        return friendly_end_date;
    }

    public String getFriendly_timing_str() {
        return friendly_timing_str;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public String getEnd_time() {
        return end_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public int getIs_active() {
        return is_active;
    }

    public String getDate_added() {
        return date_added;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public List<Object> getRestaurants() {
        return restaurants;
    }

    public int getIs_valid() {
        return is_valid;
    }

    public String getShare_url() {
        return share_url;
    }

    public int getShow_share_url() {
        return show_share_url;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDisplay_time() {
        return display_time;
    }

    public String getDisplay_date() {
        return display_date;
    }

    public int getIs_end_time_set() {
        return is_end_time_set;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public int getEvent_category() {
        return event_category;
    }

    public String getEvent_category_name() {
        return event_category_name;
    }

    public String getBook_link() {
        return book_link;
    }

    public List<Type> getTypes() {
        return types;
    }

    public ShareData getShare_data() {
        return share_data;
    }
}
