package com.example.zomatoproblem.apis.models.geoCodeResponse;

public class NearbyRestaurant{
   public com.example.zomatoproblem.apis.models.geoCodeResponse.Restaurant restaurant;

    public com.example.zomatoproblem.apis.models.geoCodeResponse.Restaurant getRestaurant() {
        return restaurant;
    }
}
