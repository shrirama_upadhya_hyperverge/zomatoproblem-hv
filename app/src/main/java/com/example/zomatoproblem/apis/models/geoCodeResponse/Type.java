package com.example.zomatoproblem.apis.models.geoCodeResponse;

public class Type{
    public String name;
    public String color;

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}
