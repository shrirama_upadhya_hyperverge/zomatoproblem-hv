package com.example.zomatoproblem.apis.models.geoCodeResponse;

public class RatingObj{
    public Title title;
    public com.example.zomatoproblem.apis.models.geoCodeResponse.BgColor bg_color;

    public Title getTitle() {
        return title;
    }

    public com.example.zomatoproblem.apis.models.geoCodeResponse.BgColor getBg_color() {
        return bg_color;
    }
}
