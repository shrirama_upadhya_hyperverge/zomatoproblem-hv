package com.example.zomatoproblem.apis.models.locationDetailsResponse;

import java.util.List;

public class POJOLocationDetailsResponseBody{
    public String popularity;
    public String nightlife_index;
    public List<String> nearby_res;
    public List<String> top_cuisines;
    public String popularity_res;
    public String nightlife_res;
    public String subzone;
    public int subzone_id;
    public String city;
    public com.example.zomatoproblem.apis.models.locationDetailsResponse.Location location;
    public int num_restaurant;
    public List<BestRatedRestaurant> best_rated_restaurant;

    public String getPopularity() {
        return popularity;
    }

    public String getNightlife_index() {
        return nightlife_index;
    }

    public List<String> getNearby_res() {
        return nearby_res;
    }

    public List<String> getTop_cuisines() {
        return top_cuisines;
    }

    public String getPopularity_res() {
        return popularity_res;
    }

    public String getNightlife_res() {
        return nightlife_res;
    }

    public String getSubzone() {
        return subzone;
    }

    public int getSubzone_id() {
        return subzone_id;
    }

    public String getCity() {
        return city;
    }

    public com.example.zomatoproblem.apis.models.locationDetailsResponse.Location getLocation() {
        return location;
    }

    public int getNum_restaurant() {
        return num_restaurant;
    }

    public List<BestRatedRestaurant> getBest_rated_restaurant() {
        return best_rated_restaurant;
    }
}

