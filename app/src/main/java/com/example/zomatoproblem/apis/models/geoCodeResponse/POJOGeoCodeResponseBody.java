package com.example.zomatoproblem.apis.models.geoCodeResponse;

import java.util.List;

public class POJOGeoCodeResponseBody {
    public Location location;
    public com.example.zomatoproblem.apis.models.geoCodeResponse.Popularity popularity;
    public String link;
    public List<NearbyRestaurant> nearby_restaurants;

    public Location getLocation() {
        return location;
    }

    public com.example.zomatoproblem.apis.models.geoCodeResponse.Popularity getPopularity() {
        return popularity;
    }

    public String getLink() {
        return link;
    }

    public List<NearbyRestaurant> getNearby_restaurants() {
        return nearby_restaurants;
    }
}



