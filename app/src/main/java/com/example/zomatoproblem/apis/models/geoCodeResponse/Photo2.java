package com.example.zomatoproblem.apis.models.geoCodeResponse;

public class Photo2{
    public String url;
    public String thumb_url;
    public int order;
    public String md5sum;
    public int id;
    public int photo_id;
    public Object uuid;
    public String type;

    public String getUrl() {
        return url;
    }

    public String getThumb_url() {
        return thumb_url;
    }

    public int getOrder() {
        return order;
    }

    public String getMd5sum() {
        return md5sum;
    }

    public int getId() {
        return id;
    }

    public int getPhoto_id() {
        return photo_id;
    }

    public Object getUuid() {
        return uuid;
    }

    public String getType() {
        return type;
    }
}
