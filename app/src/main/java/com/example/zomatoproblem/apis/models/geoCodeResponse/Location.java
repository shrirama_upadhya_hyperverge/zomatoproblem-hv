package com.example.zomatoproblem.apis.models.geoCodeResponse;

public class Location{
    public String entity_type;
    public int entity_id;
    public String title;
    public String latitude;
    public String longitude;
    public int city_id;
    public String city_name;
    public int country_id;
    public String country_name;
    public String address;
    public String locality;
    public String city;
    public String zipcode;
    public String locality_verbose;

    public String getEntity_type() {
        return entity_type;
    }

    public int getEntity_id() {
        return entity_id;
    }

    public String getTitle() {
        return title;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public int getCity_id() {
        return city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public int getCountry_id() {
        return country_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public String getAddress() {
        return address;
    }

    public String getLocality() {
        return locality;
    }

    public String getCity() {
        return city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getLocality_verbose() {
        return locality_verbose;
    }
}
