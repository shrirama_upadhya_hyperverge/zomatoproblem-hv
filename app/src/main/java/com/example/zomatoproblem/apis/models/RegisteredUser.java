package com.example.zomatoproblem.apis.models;

public class RegisteredUser {
    String Name, ID, Image;

    public RegisteredUser() {
    }

    public RegisteredUser(String name, String ID, String image) {
        Name = name;
        this.ID = ID;
        Image = image;
    }

    public String getName() {
        return Name;
    }

    public String getID() {
        return ID;
    }

    public String getImage() {
        return Image;
    }
}
