package com.example.zomatoproblem.apis.models.geoCodeResponse;

import java.util.List;

public class Popularity{
    public String popularity;
    public String nightlife_index;
    public List<String> nearby_res;
    public List<String> top_cuisines;
    public String popularity_res;
    public String nightlife_res;
    public String subzone;
    public int subzone_id;
    public String city;

    public String getPopularity() {
        return popularity;
    }

    public String getNightlife_index() {
        return nightlife_index;
    }

    public List<String> getNearby_res() {
        return nearby_res;
    }

    public List<String> getTop_cuisines() {
        return top_cuisines;
    }

    public String getPopularity_res() {
        return popularity_res;
    }

    public String getNightlife_res() {
        return nightlife_res;
    }

    public String getSubzone() {
        return subzone;
    }

    public int getSubzone_id() {
        return subzone_id;
    }

    public String getCity() {
        return city;
    }
}
