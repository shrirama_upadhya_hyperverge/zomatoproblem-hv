package com.example.zomatoproblem.apis.models.locationDetailsResponse;

public class BestRatedRestaurant implements Comparable<BestRatedRestaurant>{
    public com.example.zomatoproblem.apis.models.locationDetailsResponse.Restaurant restaurant;

    public com.example.zomatoproblem.apis.models.locationDetailsResponse.Restaurant getRestaurant() {
        return restaurant;
    }

    @Override
    public int compareTo(BestRatedRestaurant o) {
        if (Double.parseDouble(restaurant.getUser_rating().getAggregate_rating()) > Double.parseDouble(o.getRestaurant().getUser_rating().getAggregate_rating())) {
            return 1;
        }
        else if (Double.parseDouble(restaurant.getUser_rating().getAggregate_rating()) <  Double.parseDouble(o.getRestaurant().getUser_rating().getAggregate_rating())) {
            return -1;
        }
        else {
            return 0;
        }
    }
}
