package com.example.zomatoproblem.apis.api;

import com.example.zomatoproblem.apis.models.ComparisonResult;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface HypervergeAPI {
    @Multipart
    @POST("photo/verifyPair")
    Call<ComparisonResult> compare(@Part MultipartBody.Part selfie,
                                   @Part MultipartBody.Part id);

    @Multipart
    @POST("readKYC")
    Call<JsonObject> getKYC(@Part MultipartBody.Part image);

    @Multipart
    @POST("photo/verifyPair")
    Call<ComparisonResult> compareSelfies(@Part MultipartBody.Part selfie,
                                   @Part MultipartBody.Part selfie2);

}
