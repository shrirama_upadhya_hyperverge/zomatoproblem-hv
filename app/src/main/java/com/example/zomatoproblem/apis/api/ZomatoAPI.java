package com.example.zomatoproblem.apis.api;

import com.example.zomatoproblem.apis.models.geoCodeResponse.POJOGeoCodeResponseBody;
import com.example.zomatoproblem.apis.models.locationDetailsResponse.POJOLocationDetailsResponseBody;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ZomatoAPI {
    @GET("geocode")
    Call<POJOGeoCodeResponseBody> getGeoCode(@QueryMap Map<String, Double> options);

    @GET("location_details")
    Call<POJOLocationDetailsResponseBody> getLocationDetails(@Query("entity_id") int entity_id,
                                                             @Query("entity_type") String entity_type);
}
