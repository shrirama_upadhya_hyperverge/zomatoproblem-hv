package com.example.zomatoproblem.apis.models.locationDetailsResponse;

public class HasMenuStatus{
    public Object delivery;
    public int takeaway;

    public Object getDelivery() {
        return delivery;
    }

    public int getTakeaway() {
        return takeaway;
    }
}
