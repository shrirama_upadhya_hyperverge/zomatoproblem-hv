package com.example.zomatoproblem.apis.models.locationDetailsResponse;

public class RatingObj{
    public Title title;
    public com.example.zomatoproblem.apis.models.locationDetailsResponse.BgColor bg_color;

    public Title getTitle() {
        return title;
    }

    public com.example.zomatoproblem.apis.models.locationDetailsResponse.BgColor getBg_color() {
        return bg_color;
    }
}
