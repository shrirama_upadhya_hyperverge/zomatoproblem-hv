package com.example.zomatoproblem.apis.models.locationDetailsResponse;

import java.util.List;

public class AllReviews{
    public List<Review> reviews;

    public List<Review> getReviews() {
        return reviews;
    }
}
