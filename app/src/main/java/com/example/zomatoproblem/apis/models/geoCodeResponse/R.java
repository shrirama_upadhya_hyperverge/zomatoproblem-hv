package com.example.zomatoproblem.apis.models.geoCodeResponse;

public class R{
    public int res_id;
    public boolean is_grocery_store;
    public HasMenuStatus has_menu_status;

    public int getRes_id() {
        return res_id;
    }

    public boolean isIs_grocery_store() {
        return is_grocery_store;
    }

    public HasMenuStatus getHas_menu_status() {
        return has_menu_status;
    }
}
