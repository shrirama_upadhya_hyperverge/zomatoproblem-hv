package com.example.zomatoproblem.apis.models;
import com.google.gson.annotations.SerializedName;

public class ComparisonResult {
    String status, statusCode;
    @SerializedName("result")
    CompareOutput result;

    public ComparisonResult(String status, String statusCode, CompareOutput result) {
        this.status = status;
        this.statusCode = statusCode;
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public CompareOutput getResult() {
        return result;
    }
}
