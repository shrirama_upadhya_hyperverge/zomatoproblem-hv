package com.example.zomatoproblem.apis.models;

import com.google.gson.annotations.SerializedName;

public class CompareOutput{
    int conf;
    @SerializedName("match-score")
    int score;
    String match;
    @SerializedName("to-be-reviewed")
    String toReview;

    public CompareOutput(int conf, int score, String match, String toReview) {
        this.conf = conf;
        this.score = score;
        this.match = match;
        this.toReview = toReview;
    }

    public int getConf() {
        return conf;
    }

    public int getScore() {
        return score;
    }

    public String getMatch() {
        return match;
    }

    public String getToReview() {
        return toReview;
    }
}

