package com.example.zomatoproblem.apis.models.locationDetailsResponse;

public class BgColor{
    public String type;
    public String tint;

    public String getType() {
        return type;
    }

    public String getTint() {
        return tint;
    }
}
