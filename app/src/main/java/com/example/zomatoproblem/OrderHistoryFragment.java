package com.example.zomatoproblem;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OrderHistoryFragment extends Fragment {

    public OrderHistoryFragment() {
        // Required empty public constructor
    }

    private LinearLayout linearLayout;
    private OrderDatabase mDb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_order_history, container, false);
        linearLayout = root.findViewById(R.id.linear_layout_for_order_history);
        mDb = OrderDatabase.getInstance(getContext());

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                List<Order> ordersList = mDb.orderDao().getOrdersList();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        for(Order order: ordersList){
                            View orderCard = getLayoutInflater().inflate(R.layout.order_card, null, false);
                            TextView restaurantNameTextView = orderCard.findViewById(R.id.restaurant_name_text_view);
                            TextView itemsTextView = orderCard.findViewById(R.id.items_text_view);
                            TextView dateTextView = orderCard.findViewById(R.id.date_text_view);
                            TextView priceTextView = orderCard.findViewById(R.id.price_text_view);

                            String restaurantName = order.getRestaurantName();
                            String items = order.getItems();
                            Date orderDate = order.getOrderDate();
                            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            String formattedDate = dateFormat.format(orderDate);
                            int amount = order.getAmount();

                            restaurantNameTextView.setText(restaurantName);
                            itemsTextView.setText(items);
                            dateTextView.setText(formattedDate);
                            priceTextView.setText("₹" + amount);

                            linearLayout.addView(orderCard);
                        }
                    }
                });


            }
        });


        // Inflate the layout for this fragment
        return root;
    }
}