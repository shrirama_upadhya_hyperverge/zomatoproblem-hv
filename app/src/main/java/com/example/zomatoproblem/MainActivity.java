package com.example.zomatoproblem;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button registerButton, signInButton;
    private static final String SHARED_PREF_NAME = "panNo";
    private static final String KEY_NAME = "key_panNo";
    private static final String SHARED_PREF_LOGIN = "logIn";
    private static final String KEY_LOGIN = "key_logIn";
    private SharedPreferences sp, sp2;

    public static MainActivity mainActivity;

    public static MainActivity getInstance(){
        return mainActivity;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = this;
        /*View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/
        setContentView(R.layout.activity_main);
        registerButton = findViewById(R.id.registerButton);
        signInButton = findViewById(R.id.signInButton);
        sp = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        sp2 = getSharedPreferences(SHARED_PREF_LOGIN,MODE_PRIVATE);
        String pan = sp.getString(KEY_NAME, null);
        boolean logIn = sp2.getBoolean(KEY_LOGIN,false);
        Log.e("sharedPref", pan + "  : login " + logIn);

        if(logIn){
            startActivity(new Intent(this, HomeScreen.class));
            finish();
        }
        ImageView img = (ImageView)findViewById(R.id.imgViewFadeIn);
        Animation aniFade = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
        img.startAnimation(aniFade);

        TextView tv = findViewById(R.id.textViewFadeIn);
        Animation tvFade = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
        tv.startAnimation(tvFade);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                TextView textView = findViewById(R.id.textViewInsideFadeIn);
                textView.setVisibility(View.VISIBLE);
                Animation tvFade2 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
                registerButton.setVisibility(View.VISIBLE);
                Animation regFade = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
                signInButton.setVisibility(View.VISIBLE);
                Animation signFade = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
                textView.startAnimation(tvFade2);
                registerButton.startAnimation(regFade);
                signInButton.startAnimation(signFade);
            }
        }, 1000);
        registerButton.setOnClickListener(v->{
            if(pan != null){
                new AlertDialog.Builder(this)
                        .setTitle("Already registered")
                        .setMessage("User with PAN : " + pan + " has already registered. Are you sure you want to register?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
            else{
                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
//                finish();
            }
        });
        signInButton.setOnClickListener(v->{
            if(pan!=null){
                Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                intent.putExtra("panNo",pan);
                startActivity(intent);
//                finish();
            }
            else{
                new AlertDialog.Builder(this)
                        .setTitle("Please complete Registration")
                        .setMessage("Could not find any registered user in this device. Please register and then try to sign in")
                        /*.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)*/
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }
}