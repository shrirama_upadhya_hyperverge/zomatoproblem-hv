package com.example.zomatoproblem;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

public class SignInActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_main);
        /*View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/
        String panNo = getIntent().getStringExtra("panNo");
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent = new Intent(SignInActivity.this, com.example.zomatoproblem.signIn.Camera2Activity.class);
        } else {
            intent = new Intent(SignInActivity.this, com.example.zomatoproblem.signIn.CameraActivity.class);
        }
        intent.putExtra("panNo", panNo);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
//                finish();
            }
        }, 200);
        finish();
    }
}