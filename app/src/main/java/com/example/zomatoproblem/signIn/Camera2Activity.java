package com.example.zomatoproblem.signIn;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.util.SparseArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.zomatoproblem.HomeScreen;
import com.example.zomatoproblem.R;
import com.example.zomatoproblem.SignInActivity;
import com.example.zomatoproblem.apis.api.HypervergeAPI;
import com.example.zomatoproblem.apis.models.CompareOutput;
import com.example.zomatoproblem.apis.models.ComparisonResult;
import com.example.zomatoproblem.apis.models.RegisteredUser;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class Camera2Activity extends AppCompatActivity {

    final int CAMERA_REQUEST_CODE = 1;
    private CameraManager cameraManager;
    private int cameraFacing;
    private TextureView.SurfaceTextureListener surfaceTextureListener;
    private String cameraId;
    private HandlerThread backgroundThread;
    private Size previewSize;
    private Handler backgroundHandler;
    private CameraDevice.StateCallback stateCallback;
    private CameraDevice cameraDevice;
    private TextureView textureView;
    private CameraCaptureSession cameraCaptureSession;
    private CaptureRequest.Builder captureRequestBuilder;
    private CaptureRequest captureRequest;
    private FloatingActionButton captureButton;
    private ImageView imageView;

    private final StorageReference storageRef = FirebaseStorage.getInstance().getReference("Images");
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Image imageFromFirebase;
    private static final String SHARED_PREF_LOGIN = "logIn";
    private static final String KEY_LOGIN = "key_logIn";
    private final String baseURLForComparison = "https://ind-faceid.hyperverge.co/v1/";
    private HypervergeAPI hypervergeAPIForComparison;
    private boolean matched = false;
    private OkHttpClient client;
    private ProgressBar progressBar;
    private String pan = "empty";

    public static Camera2Activity camera2Activity;
    public static Camera2Activity getInstance(){
        return camera2Activity;
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        camera2Activity = this;
        /*View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/
        setContentView(R.layout.activity_sign_in);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, CAMERA_REQUEST_CODE);
        pan = getIntent().getStringExtra("panNo");

        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        cameraFacing = CameraCharacteristics.LENS_FACING_FRONT;

        textureView = findViewById(R.id.textureViewSignIn);
        captureButton = findViewById(R.id.captureSignIn);
        imageView = findViewById(R.id.imageViewSignIn);
        imageView.setVisibility(View.GONE);
        textureView.setVisibility(View.VISIBLE);
        progressBar = findViewById(R.id.progress_circular_signIn);
        DocumentReference docRef = db.collection("RegisteredUsers").document(pan);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                RegisteredUser registeredUser = documentSnapshot.toObject(RegisteredUser.class);
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //code to do the HTTP request
                        InputStream input = null;
                        try {
                            input = new java.net.URL(registeredUser.getImage()).openStream();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Bitmap myBitmap = BitmapFactory.decodeStream(input);
                        File pictureFileDir = new File(getCacheDir() + File.separator + "images");
                        Log.d("path", "" + pictureFileDir);
                        pictureFileDir.mkdirs();
                        String filename = pictureFileDir.getPath() + File.separator + "selfieFromFirebase.jpg";
                        File pictureFile = new File(filename);
                        try {
                            FileOutputStream fos = new FileOutputStream(pictureFile);
                            myBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                            fos.close();
                        } catch (Exception error) {
                            Log.d("tag", "File" + filename + "not saved: "
                                    + error.getMessage());
                            Toast.makeText(Camera2Activity.this, "Image could not be saved.",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
                thread.start();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("onFailList", e.getMessage());
            }
        });
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("appId", "857acb")
                        .header("appKey", "e0d888e703691bccbf92")
                        .header("Content-Type", "multipart/form-data")
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });
        client = httpClient.build();

        surfaceTextureListener = new TextureView.SurfaceTextureListener() {

            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
                setUpCamera();
                openCamera();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

            }
        };


        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ProcessImageTask().execute();
            }
        });


        stateCallback = new CameraDevice.StateCallback() {
            @Override
            public void onOpened(CameraDevice cameraDevice) {
                Camera2Activity.this.cameraDevice = cameraDevice;
                createPreviewSession();
            }

            @Override
            public void onDisconnected(CameraDevice cameraDevice) {
                cameraDevice.close();
                Camera2Activity.this.cameraDevice = null;
            }

            @Override
            public void onError(CameraDevice cameraDevice, int error) {
                cameraDevice.close();
                Camera2Activity.this.cameraDevice = null;
            }
        };
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setUpCamera() {
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cameraCharacteristics =
                        cameraManager.getCameraCharacteristics(cameraId);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) ==
                        cameraFacing) {
                    StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(
                            CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                    previewSize = streamConfigurationMap.getOutputSizes(SurfaceTexture.class)[0];
                    this.cameraId = cameraId;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    private void openCamera() {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                cameraManager.openCamera(cameraId, stateCallback, backgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void openBackgroundThread() {
        backgroundThread = new HandlerThread("camera_background_thread");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    private void closeCamera() throws IOException {
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }

        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    private void closeBackgroundThread() {
        if (backgroundHandler != null) {
            backgroundThread.quitSafely();
            backgroundThread = null;
            backgroundHandler = null;
        }
    }


    private void lock() {
        try {
            cameraCaptureSession.capture(captureRequestBuilder.build(),
                    null, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void unlock() {
        try {
            cameraCaptureSession.setRepeatingRequest(captureRequestBuilder.build(),
                    null, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void createPreviewSession() {
        try {
            SurfaceTexture surfaceTexture = textureView.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(previewSurface);
            if(cameraDevice == null){
                startActivity(getIntent());
            }

            cameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                            if (cameraDevice == null) {
                                return;
                            }

                            try {
                                captureRequest = captureRequestBuilder.build();
                                Camera2Activity.this.cameraCaptureSession = cameraCaptureSession;
                                Camera2Activity.this.cameraCaptureSession.setRepeatingRequest(captureRequest,
                                        null, backgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                            startActivity(getIntent());
                        }
                    }, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        openBackgroundThread();
        if (textureView.isAvailable()) {
            setUpCamera();
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(surfaceTextureListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            closeCamera();
        } catch (IOException e) {
            e.printStackTrace();
        }
        closeBackgroundThread();
    }

    private class ProcessImageTask extends AsyncTask<Void, Void, Void> {

        File pictureFileDir, pictureFile;
        String photoFile, filename;
        Bitmap tempBitmap;

        @Override
        protected void onPreExecute() {
            captureButton.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            Bitmap textureViewBitmap = textureView.getBitmap();
            imageView.setImageBitmap(textureViewBitmap);
            imageView.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {


            pictureFileDir = new File(getCacheDir() + File.separator + "images");
            Log.d("path", "" + pictureFileDir);

            pictureFileDir.mkdirs();
            photoFile = "selfieFromSignIn.jpg";
            filename = pictureFileDir.getPath() + File.separator + photoFile;
            pictureFile = new File(filename);
            Log.d("path", "" + pictureFileDir);

            Bitmap mBitmap = textureView.getBitmap();
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                Bitmap selfieBitmap = textureView.getBitmap();
                selfieBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                Log.d("tag", "File" + filename + "not saved: "
                        + e.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Camera2Activity.this, "Image could not be saved.",
                                Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
            Paint mPaint = new Paint();
            mPaint.setStrokeWidth(5);
            mPaint.setColor(Color.RED);
            mPaint.setStyle(Paint.Style.STROKE);

            tempBitmap = Bitmap.createBitmap(mBitmap.getWidth(),
                    mBitmap.getHeight(),
                    Bitmap.Config.RGB_565);

            Canvas tempCanvas = new Canvas(tempBitmap);
            tempCanvas.drawBitmap(mBitmap, 0, 0, null);

            FaceDetector faceDetector = new FaceDetector.Builder(getApplicationContext())
                    .setTrackingEnabled(false)
                    .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                    .build();
            if (!faceDetector.isOperational()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new AlertDialog.Builder(getApplicationContext()).setMessage("Could not set up faceDetector").show();

                    }
                });
                return null;
            }
            Frame frame = new Frame.Builder().setBitmap(mBitmap).build();
            SparseArray<Face> faces = faceDetector.detect(frame);
            if (faces.size() <= 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Camera2Activity.this,
                                "No faces detected! Please make sure you don't have anything covering your face",
                                Toast.LENGTH_SHORT)
                                .show();
                        onBackPressed();
                    }
                });

            }

            for (int i = 0; i < faces.size(); ++i) {
                Face thisFace = faces.valueAt(i);
                float x1 = thisFace.getPosition().x;
                float y1 = thisFace.getPosition().y;
                float x2 = x1 + thisFace.getWidth();
                float y2 = y1 + thisFace.getHeight();
                        /*for (Landmark landmark : face.getLandmarks()) {
                                int cx = (int) (landmark.getPosition().x * scale);
                                int cy = (int) (landmark.getPosition().y * scale);
                                tempCanvas.drawCircle(cx, cy, 10, paint);
                        }*/
                tempCanvas.drawRoundRect(new RectF(x1, y1, x2, y2), 2, 2, mPaint);
            }

            faceDetector.release();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            imageView.setImageDrawable(new BitmapDrawable(getResources(), tempBitmap));
            imageView.setVisibility(View.VISIBLE);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseURLForComparison)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            hypervergeAPIForComparison = retrofit.create(HypervergeAPI.class);
            Uri selfieFromFirebase = Uri.fromFile(new File(getCacheDir() + File.separator + "images" + File.separator + "selfieFromFirebase.jpg"));
            Uri selfieFromSignIn = Uri.fromFile(new File(getCacheDir() + File.separator + "images" + File.separator + "selfieFromSignIn.jpg"));
            Log.e("App", "" + selfieFromFirebase);
            File selfieFile = new File(selfieFromSignIn.getPath());
            File idFile = new File(selfieFromFirebase.getPath());
            RequestBody selfieRequestFile = RequestBody.create(MediaType.parse("image/*"),
                    selfieFile);
            RequestBody idRequestFile = RequestBody.create(MediaType.parse("image/*"),
                    idFile);
            MultipartBody.Part selfieBody =
                    MultipartBody.Part.createFormData("selfie", selfieFile.getName(), selfieRequestFile);
            MultipartBody.Part idBody =
                    MultipartBody.Part.createFormData("selfie2", idFile.getName(), idRequestFile);
            Call<ComparisonResult> call = hypervergeAPIForComparison.compareSelfies(selfieBody, idBody);
            call.enqueue(new Callback<ComparisonResult>() {
                @Override
                public void onResponse(Call<ComparisonResult> call, Response<ComparisonResult> response) {
                    if (!response.isSuccessful()) {
                        Log.e("App", "Code : " + response.code() + " : " + response.message() + "\n Response Headers" + response.headers());
                        Toast.makeText(Camera2Activity.this, "Sorry for the technical error : " + response.message(), Toast.LENGTH_SHORT).show();
                        finish();
                        return;
                    }
                    ComparisonResult comaprisonResult = response.body();
                    String content = "";
                    content += "Status : " + comaprisonResult.getStatus() + "\n";
                    content += "StatusCode : " + comaprisonResult.getStatusCode() + "\n";
                    CompareOutput output = comaprisonResult.getResult();
                    content += "Conf : " + output.getConf() + "\n";
                    content += "Match : " + output.getMatch() + "\n";
                    content += "Score : " + output.getScore() + "\n";
                    content += "To-be-reviewed : " + output.getToReview() + "\n";
                    Log.e("App", content);
                    if (output.getMatch().equalsIgnoreCase("yes")) {
                        matched = true;
                    }
                    if (matched) {
                        SharedPreferences sp2 = getSharedPreferences(SHARED_PREF_LOGIN, MODE_PRIVATE);
                        SharedPreferences.Editor e2 = sp2.edit();
                        e2.putBoolean(KEY_LOGIN, true);
                        e2.apply();
                        Toast.makeText(Camera2Activity.this, "Successful signIn", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Camera2Activity.this, HomeScreen.class));
                    } else {
                        Toast.makeText(Camera2Activity.this, "Image present in Firebase and Selfie don't match, Please try again", Toast.LENGTH_SHORT).show();
                        startActivity(getIntent());
                    }
                    progressBar.setVisibility(View.GONE);
                    finish();
                }

                @Override
                public void onFailure(Call<ComparisonResult> call, Throwable t) {
                    Log.e("App", t.getMessage());
                    Toast.makeText(Camera2Activity.this, "Sorry for the technical error : " + t.getMessage()+  ". Reloading now", Toast.LENGTH_SHORT).show();
                    finish();
                    startActivity(getIntent());
                }
            });
        }
    }

}
