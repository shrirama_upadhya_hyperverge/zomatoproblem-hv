package com.example.zomatoproblem.signIn;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zomatoproblem.HomeScreen;
import com.example.zomatoproblem.R;
import com.example.zomatoproblem.SignInActivity;
import com.example.zomatoproblem.apis.api.HypervergeAPI;
import com.example.zomatoproblem.apis.models.CompareOutput;
import com.example.zomatoproblem.apis.models.ComparisonResult;
import com.example.zomatoproblem.apis.models.RegisteredUser;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CameraActivity extends AppCompatActivity {

    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private Context myContext;
    private LinearLayout cameraPreview;
    private ImageButton captureButton, flipCameraButton;
    private boolean cameraFront = false;
    public static Bitmap bitmap;

    private final StorageReference storageRef = FirebaseStorage.getInstance().getReference("Images");
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Image imageFromFirebase;
    private static final String SHARED_PREF_LOGIN = "logIn";
    private static final String KEY_LOGIN = "key_logIn";
    private final String baseURLForComparison ="https://ind-faceid.hyperverge.co/v1/";
    private HypervergeAPI hypervergeAPIForComparison;
    private boolean matched = false;
    private OkHttpClient client;
    private ProgressBar progressBar;
    private String pan = "empty";
    private ImageView imageView;
    public static CameraActivity cameraActivity;
    public static CameraActivity getInstance(){
        return cameraActivity;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        cameraActivity = this;
        /*View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;
        pan = getIntent().getStringExtra("panNo");

        imageView = findViewById(R.id.imageViewSignIn);
        cameraPreview = (LinearLayout) findViewById(R.id.preview_linear_layout_signIn);
        captureButton = (ImageButton) findViewById(R.id.captureSignIn);
//        flipCameraButton = (ImageButton) findViewById(R.id.flipCameraSignIn);
        cameraPreview.setVisibility(View.VISIBLE);
        progressBar = findViewById(R.id.progress_circular_signIn);
        DocumentReference docRef = db.collection("RegisteredUsers").document(pan);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                RegisteredUser registeredUser = documentSnapshot.toObject(RegisteredUser.class);
                Thread thread = new Thread(new Runnable(){
                    @Override
                    public void run(){
                        //code to do the HTTP request
                        InputStream input = null;
                        try {
                            input = new java.net.URL(registeredUser.getImage()).openStream();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Bitmap myBitmap = BitmapFactory.decodeStream(input);
                        File pictureFileDir = new File(getCacheDir()+File.separator+"images");
                        Log.d("path",""+pictureFileDir);
                        pictureFileDir.mkdirs();
                        String filename = pictureFileDir.getPath() + File.separator + "selfieFromFirebase.jpg";
                        File pictureFile = new File(filename);
                        try {
                            FileOutputStream fos = new FileOutputStream(pictureFile);
                            myBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                            fos.close();
                        } catch (Exception error) {
                            Log.d("tag", "File" + filename + "not saved: "
                                    + error.getMessage());
                            Toast.makeText(CameraActivity.this, "Image could not be saved.",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
                thread.start();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("onFailList", e.getMessage());
            }
        });
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("appId", "857acb")
                        .header("appKey","e0d888e703691bccbf92")
                        .header("Content-Type", "multipart/form-data")
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });
        client = httpClient.build();

        mCamera =  Camera.open();
        mCamera.setDisplayOrientation(90);
        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.takePicture(null, null, mPicture);
            }
        });

        flipCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the number of cameras
                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {
                    //release the old camera instance
                    //switch camera, from the front and the back and vice versa

                    releaseCamera();
                    chooseCamera();
                } else {

                }
            }
        });

        mCamera.startPreview();

    }


    private int findFrontFacingCamera() {

        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;

    }


    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;

            }

        }
        return cameraId;
    }


    public void onResume() {

        super.onResume();
        if(mCamera == null) {
            mCamera = Camera.open();
            mCamera.setDisplayOrientation(90);
            mPicture = getPictureCallback();
            mPreview.refreshCamera(mCamera);
            Log.d("nu", "null");
        }else {
            Log.d("nu","no null");
        }

    }


    public void chooseCamera() {
        //if the camera preview is the front
        if (cameraFront) {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview

                mCamera = Camera.open(cameraId);
                mCamera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                mCamera = Camera.open(cameraId);
                mCamera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        //when on Pause, release camera in order to be used from other applications
        releaseCamera();
    }


    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                captureButton.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);

                File pictureFileDir = new File(getCacheDir()+File.separator+"images");
                Log.d("path",""+pictureFileDir);

                pictureFileDir.mkdirs();
                String photoFile = "selfieFromSignIn.jpg";
                String filename = pictureFileDir.getPath() + File.separator + photoFile;
                File pictureFile = new File(filename);
                Log.d("path",""+pictureFileDir);

                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    Bitmap selfieBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    Bitmap mBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    Paint mPaint = new Paint();
                    mPaint.setStrokeWidth(5);
                    mPaint.setColor(Color.RED);
                    mPaint.setStyle(Paint.Style.STROKE);

                    Bitmap tempBitmap = Bitmap.createBitmap(mBitmap.getWidth(),
                            mBitmap.getHeight(),
                            Bitmap.Config.RGB_565);

                    Canvas tempCanvas = new Canvas(tempBitmap);
                    tempCanvas.drawBitmap(mBitmap,0,0,null);

                    FaceDetector faceDetector = new FaceDetector.Builder(getApplicationContext())
                            .setTrackingEnabled(false)
                            .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                            .build();
                    if(!faceDetector.isOperational()){
                        new AlertDialog.Builder(CameraActivity.this).setMessage("Could not set up faceDetector").show();
                        return;
                    }
                    Frame frame = new Frame.Builder().setBitmap(mBitmap).build();
                    SparseArray<Face> faces = faceDetector.detect(frame);
                    if(faces.size() <= 0){
                        Toast.makeText(CameraActivity.this,
                                "No faces detected! Please make sure you don't have anything covering your face",
                                Toast.LENGTH_SHORT)
                                .show();
                        onBackPressed();
                    }

                    for (int i = 0; i < faces.size(); ++i) {
                        Face thisFace = faces.valueAt(i);
                        float x1 = thisFace.getPosition().x;
                        float y1 = thisFace.getPosition().y;
                        float x2 = x1 + thisFace.getWidth();
                        float y2 = y1 + thisFace.getHeight();
                        /*for (Landmark landmark : face.getLandmarks()) {
                                int cx = (int) (landmark.getPosition().x * scale);
                                int cy = (int) (landmark.getPosition().y * scale);
                                tempCanvas.drawCircle(cx, cy, 10, paint);
                        }*/
                        tempCanvas.drawRoundRect(new RectF(x1,y1,x2,y2), 2, 2, mPaint);
                    }
                    imageView.setImageDrawable(new BitmapDrawable(getResources(),tempBitmap));
                    imageView.setVisibility(View.VISIBLE);
                    faceDetector.release();
                    selfieBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.close();
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(baseURLForComparison)
                            .client(client)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    hypervergeAPIForComparison = retrofit.create(HypervergeAPI.class);
                    Uri selfieFromFirebase = Uri.fromFile(new File(getCacheDir() + File.separator + "images" + File.separator + "selfieFromFirebase.jpg"));
                    Uri selfieFromSignIn = Uri.fromFile(new File(getCacheDir() + File.separator + "images" + File.separator + "selfieFromSignIn.jpg"));
                    Log.e("App", ""+selfieFromFirebase);
                    File selfieFile = new File(selfieFromSignIn.getPath());
                    File idFile = new File(selfieFromFirebase.getPath());
                    RequestBody selfieRequestFile = RequestBody.create(MediaType.parse("image/*"),
                            selfieFile);
                    RequestBody idRequestFile = RequestBody.create(MediaType.parse("image/*"),
                            idFile);
                    MultipartBody.Part selfieBody =
                            MultipartBody.Part.createFormData("selfie", selfieFile.getName(), selfieRequestFile);
                    MultipartBody.Part idBody =
                            MultipartBody.Part.createFormData("selfie2", idFile.getName(), idRequestFile);
                    Call<ComparisonResult> call = hypervergeAPIForComparison.compareSelfies(selfieBody,idBody);
                    call.enqueue(new Callback<ComparisonResult>() {
                        @Override
                        public void onResponse(Call<ComparisonResult> call, Response<ComparisonResult> response) {
                            if(!response.isSuccessful()){
                                Log.e("App","Code : " + response.code() + " : "+ response.message() + "\n Response Headers" +response.headers());
                                return;
                            }
                            ComparisonResult comaprisonResult = response.body();
                            String content= "";
                            content += "Status : " + comaprisonResult.getStatus() + "\n";
                            content += "StatusCode : " + comaprisonResult.getStatusCode() + "\n";
                            CompareOutput output = comaprisonResult.getResult();
                            content += "Conf : " + output.getConf() + "\n";
                            content += "Match : " + output.getMatch() + "\n";
                            content += "Score : " + output.getScore() + "\n";
                            content += "To-be-reviewed : " + output.getToReview() + "\n";
                            Log.e("App", content);
                            if(output.getMatch().equalsIgnoreCase("yes")){
                                matched = true;
                            }
                            if(matched){
                                SharedPreferences sp2 = getSharedPreferences(SHARED_PREF_LOGIN, MODE_PRIVATE);
                                SharedPreferences.Editor e2 = sp2.edit();
                                e2.putBoolean(KEY_LOGIN, true);
                                e2.apply();
                                Toast.makeText(CameraActivity.this, "Successful signIn", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(CameraActivity.this, HomeScreen.class));
                                finish();
                            }
                            else {
                                Toast.makeText(CameraActivity.this, "Image present in Firebase and Selfie don't match, Please try again", Toast.LENGTH_SHORT).show();
                                startActivity(getIntent());
                            }
                            progressBar.setVisibility(View.INVISIBLE);

                        }
                        @Override
                        public void onFailure(Call<ComparisonResult> call, Throwable t) {
                            Log.e("App", t.getMessage());
                            Toast.makeText(CameraActivity.this, "Sorry for the technical error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(getIntent());
                        }
                    });
                } catch (Exception error) {
                    Log.d("tag", "File" + filename + "not saved: "
                            + error.getMessage());

                    Toast.makeText(CameraActivity.this, "Image could not be saved.",
                            Toast.LENGTH_LONG).show();
                }
            }
        };
        return picture;
    }
}
