package com.example.zomatoproblem;

import java.io.Serializable;

public class Dish implements Serializable {
    private final String mDishName;
    private final boolean mVeg;
    private final int mPrice;

    public Dish(String name, boolean veg, int price){
        mDishName = name;
        mVeg = veg;
        mPrice = price;
    }

    public String getDishName(){
        return mDishName;
    }

    public boolean getVeg(){
        return mVeg;
    }

    public int getPrice(){
        return mPrice;
    }
}
