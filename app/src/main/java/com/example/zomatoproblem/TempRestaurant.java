package com.example.zomatoproblem;

import java.io.Serializable;

public class TempRestaurant implements Serializable {
    private final String mName;
    private final String mId;
    private final double mRating;
    private final String mFeaturedImage;
    private final String mCuisineType;

    public TempRestaurant(String id, String name, double rating, String featuredImage, String cuisineType){
        mId = id;
        mName = name;
        mRating = rating;
        mFeaturedImage = featuredImage;
        mCuisineType = cuisineType;
    }

    public String getRestaurantName(){
        return mName;
    }

    public String getRestaurantId(){
        return mId;
    }

    public double getRating(){
        return mRating;
    }

    public String getFeaturedImage(){
        return mFeaturedImage;
    }

    public String getCuisineType(){
        return mCuisineType;
    }
}
