package com.example.zomatoproblem;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = Order.class, exportSchema = false, version = 1)
public abstract class OrderDatabase extends RoomDatabase {

    private static final String DB_NAME = "orders_db";
    private static OrderDatabase instance;

    public static synchronized OrderDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), OrderDatabase.class, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instance;
    }

    public abstract OrderDao orderDao();
}
